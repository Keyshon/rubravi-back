const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const fetch = require('node-fetch');

import User from './../Models/User';

class AuthenticationController {
  login(req, res, next) {
    const data = req.body;
    User.findOne({ name: data.name }).then(user => {
      if (!user) return res.status(404).send({ status: 'Not found' });

      bcrypt.compare(data.password, user.password, (err, isMatch) => {
        if (err)
          return res.status(400).send({ status: 'Error while comparing' });

        if (isMatch) {
          const token = jwt.sign(user.toJSON(), 'secret', {
            expiresIn: '2h'
          });

          return res.status(200).json({
            status: 'Logged in',
            token: 'Bearer ' + token
          });
        } else {
          return res.status(400).send({ status: 'Unauthorized' });
        }
      });
    });
  }

  loginBrowser(req, res, next) {
    const data = req.body;
    User.findOne({ name: data.name }).then(user => {
      if (!user) return res.redirect('/');

      bcrypt.compare(data.password, user.password, (err, isMatch) => {
        if (err) return res.redirect('/');

        if (isMatch) {
          const token = jwt.sign(user.toJSON(), 'secret', {
            expiresIn: '2h'
          });

          let data;
          fetch('http://127.0.0.1:3001/api/orders', {
            method: 'GET',
            headers: {
              Authorization: 'Bearer ' + token
            }
          })
            .then(response => response.json())
            .then(parseRes => {
              data = parseRes;
              return res.render('orders', {
                data: data
              });
            });
        } else {
          return res.redirect('/');
        }
      });
    });
  }
}

export default AuthenticationController;
