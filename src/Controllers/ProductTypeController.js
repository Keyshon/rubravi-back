import ProductType from './../Models/ProductType';
const Joi = require('joi');

class ProudctTypeController {
  index(req, res) {
    ProductType.find()
      .then(products => {
        return res.status(200).send(products);
      })
      .catch(e => res.status(400).send(e));
  }

  create(req, res) {
    const data = req.body;

    const { error } = checkProductType(data);
    if (error && error !== undefined)
      return res.status(404).send({ status: 'not found' });

    const product = new ProductType({
      name: data.name,
      costMonth: data.costMonth,
      costHalfYear: data.costHalfYear,
      costYear: data.costYear,
      supportTerm: data.supportTerm
    });

    product.save().then(() => {
      return res.status(201).send(product);
    });
  }

  read(req, res) {
    ProductType.findOne({ _id: req.params.id })
      .then(product => {
        if (product === null)
          return res.status(404).send({ status: 'not found' });
        return res.status(200).send(product);
      })
      .catch(e => res.status(404).send({ status: 'not found' }));
  }

  update(req, res) {
    const data = req.body;

    const { error } = checkProductType(data);
    if (error && error !== undefined)
      return res.status(404).send({ status: 'not found' });

    ProductType.findOneAndUpdate(
      { _id: req.params.id },
      { $set: data },
      { new: true }
    )
      .then(product => {
        return res.status(200).send(product);
      })
      .catch(e => res.status(404).send({ status: 'not found' }));
  }

  patch(req, res) {
    ProductType.findOneAndUpdate(
      { _id: req.params.id },
      { $set: req.body },
      { new: true }
    )
      .then(product => {
        return res.status(200).send(product);
      })
      .catch(e => res.status(404).send({ status: 'not found' }));
  }

  delete(req, res) {
    ProductType.deleteOne({ _id: req.params.id })
      .then(() => {
        return res.status(204).send();
      })
      .catch(e => res.status(404).send({ status: 'not found' }));
  }
}

function checkProductType(request) {
  const schema = {
    name: Joi.string()
      .min(3)
      .required(),
    costMonth: Joi.number()
      .min(0)
      .required(),
    costHalfYear: Joi.number()
      .min(0)
      .required(),
    costYear: Joi.number()
      .min(0)
      .required(),
    supportTerm: Joi.number()
      .min(0)
      .required()
  };
  return Joi.validate(request, schema);
}

export default ProudctTypeController;
