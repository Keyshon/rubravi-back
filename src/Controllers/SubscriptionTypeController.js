import SubscriptionType from './../Models/SubscriptionType';
const Joi = require('joi');

class SubscriptionTypeController {
  index(req, res) {
    SubscriptionType.find()
      .then(subscriptions => {
        return res.status(200).send(subscriptions);
      })
      .catch(e => res.status(400).send(e));
  }

  create(req, res) {
    const data = req.body;

    const { error } = checkSubscriptionType(data);
    if (error && error !== undefined)
      return res.status(404).send({ status: 'not found' });

    const subscription = new SubscriptionType({
      name: data.name,
      term: data.term
    });

    subscription.save().then(() => {
      return res.status(201).send(subscription);
    });
  }

  read(req, res) {
    SubscriptionType.findOne({ _id: req.params.id })
      .then(subscription => {
        if (subscription === null)
          return res.status(404).send({ status: 'not found' });
        return res.status(200).send(subscription);
      })
      .catch(e => res.status(404).send({ status: 'not found' }));
  }

  update(req, res) {
    const data = req.body;

    const { error } = checkSubscriptionType(data);
    if (error && error !== undefined)
      return res.status(404).send({ status: 'not found' });

    SubscriptionType.findOneAndUpdate(
      { _id: req.params.id },
      { $set: data },
      { new: true }
    )
      .then(subscription => {
        return res.status(200).send(subscription);
      })
      .catch(e => res.status(404).send({ status: 'not found' }));
  }

  patch(req, res) {
    SubscriptionType.findOneAndUpdate(
      { _id: req.params.id },
      { $set: req.body },
      { new: true }
    )
      .then(subscription => {
        return res.status(200).send(subscription);
      })
      .catch(e => res.status(404).send({ status: 'not found' }));
  }

  delete(req, res) {
    SubscriptionType.deleteOne({ _id: req.params.id })
      .then(() => {
        return res.status(204).send();
      })
      .catch(e => res.status(404).send({ status: 'not found' }));
  }
}

function checkSubscriptionType(request) {
  const schema = {
    name: Joi.string()
      .min(3)
      .required(),
    term: Joi.number()
      .min(0)
      .required()
  };
  return Joi.validate(request, schema);
}

export default SubscriptionTypeController;
