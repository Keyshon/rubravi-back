import ProductOrder from './../Models/ProductOrder';
import ProductType from './../Models/ProductType';
import SubscriptionType from './../Models/SubscriptionType';
import sendEmail from './../Utils/mailing';

const Joi = require('joi');

class ProductOrderController {
  index(req, res) {
    ProductOrder.find()
      .populate({ path: 'productType', select: 'name' })
      .populate({ path: 'subscriptionType', select: 'name' })
      .then(orders => {
        return res.status(200).send(orders);
      })
      .catch(e => res.status(400).send(e));
  }

  read(req, res) {
    ProductOrder.findOne({ _id: req.params.id })
      .then(order => {
        if (order === null)
          return res.status(404).send({ status: 'Not found' });
        return res.status(200).send(order);
      })
      .catch(e => res.status(404).send({ status: 'Not found' }));
  }

  create(req, res) {
    const data = req.body;

    const { error } = checkProductOrder(data);
    if (error && error !== undefined)
      return res.status(400).send({ status: error.details[0].message });

    let product;
    let subscription;

    ProductType.findOne({ _id: data.productType })
      .then(prod => {
        product = prod;
      })
      .catch(e =>
        res.status(400).send({ status: 'Product Type is not found' })
      );
    SubscriptionType.findOne({ _id: data.subscriptionType })
      .then(sub => {
        subscription = sub;
      })
      .catch(e =>
        res.status(400).send({ status: 'Subscription Type is not found' })
      );

    const order = new ProductOrder({
      productType: data.productType,
      userName: data.userName,
      userPhone: data.userPhone,
      userEmail: data.userEmail,
      subscriptionType: data.subscriptionType,
      subscriptionTerm: data.subscriptionTerm
    });

    order
      .save()
      .then(() => {
        sendEmail(
          'rubravi.official@gmail.com',
          'New order',
          `<h2>A new order has been received</h2>
          <p>
          <b>Name:</b> ${data.userName}<br/>
          <b>Phone:</b> ${data.userPhone}<br/>
          <b>Email:</b> ${data.userEmail}<br/>
          <b>Product:</b> ${product.name}<br/>
          <b>Subscription:</b> ${subscription.name}<br/>
          <b>Term:</b> ${data.subscriptionTerm}
          </p>`
        );
        return res.status(201).send(order);
      })
      .catch(e => res.status(400).send({ status: e }));
  }

  update(req, res) {
    const data = req.body;

    const { error } = checkProductOrder(data);
    if (error && error !== undefined)
      return res.status(404).send({ status: 'Not found' });

    ProductType.findOne({ _id: data.productType })
      .then()
      .catch(e =>
        res.status(400).send({ status: 'Product Type is not found' })
      );
    SubscriptionType.findOne({ _id: data.subscriptionType })
      .then()
      .catch(e =>
        res.status(400).send({ status: 'Subscription Type is not found' })
      );

    ProductOrder.findOneAndUpdate(
      { _id: req.params.id },
      { $set: data },
      { new: true }
    )
      .then(order => {
        return res.status(200).send(order);
      })
      .catch(e => res.status(400).send({ status: e }));
  }

  patch(req, res) {
    const data = req.body;

    if (data.productType)
      ProductType.findOne({ _id: data.productType })
        .then()
        .catch(e =>
          res.status(400).send({ status: 'Product Type is not found' })
        );
    if (data.subscriptionType)
      SubscriptionType.findOne({ _id: data.subscriptionType })
        .then()
        .catch(e =>
          res.status(400).send({ status: 'Subscription Type is not found' })
        );

    ProductOrder.findOneAndUpdate(
      { _id: req.params.id },
      { $set: data },
      { new: true }
    )
      .then(order => {
        return res.status(200).send(order);
      })
      .catch(e => res.status(400).send({ status: e }));
  }

  delete(req, res) {
    ProductOrder.deleteOne({ _id: req.params.id })
      .then(() => {
        return res.status(204).send();
      })
      .catch(e => res.status(404).send({ status: 'Not found' }));
  }
}

function checkProductOrder(request) {
  const schema = {
    productType: Joi.required(),
    userName: Joi.string()
      .min(5)
      .max(30)
      .required(),
    userPhone: Joi.string()
      .min(6)
      .max(20)
      .required(),
    userEmail: Joi.string()
      .email()
      .required(),
    subscriptionType: Joi.required(),
    subscriptionTerm: Joi.number()
      .min(0)
      .required()
  };
  return Joi.validate(request, schema);
}

export default ProductOrderController;
