import BillingPlan from '../Models/BillingPlan';
const Joi = require('joi');

class BillingPlanController {
  index(req, res) {
    BillingPlan.find()
      .then(plan => {
        return res.status(200).send(plan);
      })
      .catch(e => res.status(400).send(e));
  }

  create(req, res) {
    const data = req.body;

    const { error } = checkBillingPlan(data);
    if (error && error !== undefined)
      return res.status(404).send({ status: 'not found' });

    const plan = new BillingPlan({
      name: data.name,
      cost: data.cost
    });

    plan.save().then(() => {
      return res.status(201).send(plan);
    });
  }

  read(req, res) {
    BillingPlan.findOne({ _id: req.params.id })
      .then(plan => {
        if (plan === null) return res.status(404).send({ status: 'not found' });
        return res.status(200).send(plan);
      })
      .catch(e => res.status(400).send(e));
  }

  update(req, res) {
    const data = req.body;

    const { error } = checkBillingPlan(data);
    if (error && error !== undefined)
      return res.status(404).send({ status: 'not found' });

    BillingPlan.findOneAndUpdate(
      { _id: req.params.id },
      { $set: data },
      { new: true }
    )
      .then(plan => {
        return res.status(200).send(plan);
      })
      .catch(e => res.status(400).send(e));
  }

  patch(req, res) {
    BillingPlan.findOneAndUpdate(
      { _id: req.params.id },
      { $set: req.body },
      { new: true }
    )
      .then(plan => {
        return res.status(200).send(plan);
      })
      .catch(e => res.status(404).send({ status: 'not found' }));
  }

  delete(req, res) {
    BillingPlan.deleteOne({ _id: req.params.id })
      .then(() => {
        return res.status(204).send();
      })
      .catch(e => res.status(404).send({ status: 'not found' }));
  }
}

function checkBillingPlan(request) {
  const schema = {
    name: Joi.string()
      .min(3)
      .required(),
    cost: Joi.number()
      .positive()
      .required()
  };
  return Joi.validate(request, schema);
}

export default BillingPlanController;
