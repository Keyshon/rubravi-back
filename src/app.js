import mongoose from 'mongoose';
import express from 'express';
import bodyParser from 'body-parser';
const passport = require('passport');
const cors = require('cors');
const protect = require('./Config/Auth');
const expressLayouts = require('express-ejs-layouts');
const fetch = require('node-fetch');

import BillingPlanController from './Controllers/BillingPlanController';
import SubscriptionTypeController from './Controllers/SubscriptionTypeController';
import ProductTypeController from './Controllers/ProductTypeController';
import ProductOrderController from './Controllers/ProductOrderController';
import AuthenticationController from './Controllers/AuthenticationController';

const billingPlanController = new BillingPlanController();
const subscriptionTypeController = new SubscriptionTypeController();
const productTypeController = new ProductTypeController();
const productOrderController = new ProductOrderController();
const authenticationController = new AuthenticationController();

const app = express();

mongoose.connect('mongodb://localhost/rubravi');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());

app.use(passport.initialize());
app.use(passport.session());

require('./Config/Passport')(passport);

app.get('/api', (req, res) => {
  return res.status(200).send('Working!');
});

mongoose.set('useFindAndModify', false);

app.use(expressLayouts);
app.set('view engine', 'ejs');

app.get('/api/billingplans', billingPlanController.index);
app.get('/api/billingplans/:id', billingPlanController.read);
app.post('/api/billingplans', protect(), billingPlanController.create);
app.patch('/api/billingplans/:id', protect(), billingPlanController.patch);
app.put('/api/billingplans/:id', protect(), billingPlanController.update);
app.delete('/api/billingplans/:id', protect(), billingPlanController.delete);

app.get('/api/subscriptiontypes', subscriptionTypeController.index);
app.get('/api/subscriptiontypes/:id', subscriptionTypeController.read);
app.post(
  '/api/subscriptiontypes',
  protect(),
  subscriptionTypeController.create
);
app.patch(
  '/api/subscriptiontypes/:id',
  protect(),
  subscriptionTypeController.patch
);
app.put(
  '/api/subscriptiontypes/:id',
  protect(),
  subscriptionTypeController.update
);
app.delete(
  '/api/subscriptiontypes/:id',
  protect(),
  subscriptionTypeController.delete
);

app.get('/api/products', productTypeController.index);
app.get('/api/products/:id', productTypeController.read);
app.post('/api/products', protect(), productTypeController.create);
app.patch('/api/products/:id', protect(), productTypeController.patch);
app.put('/api/products/:id', protect(), productTypeController.update);
app.delete('/api/products/:id', protect(), productTypeController.delete);

app.get('/api/orders', protect(), productOrderController.index);
app.get('/api/orders/:id', protect(), productOrderController.read);
app.post('/api/orders/', productOrderController.create);
app.put('/api/orders/:id', protect(), productOrderController.update);
app.patch('/api/orders/:id', protect(), productOrderController.patch);
app.delete('/api/orders/:id', protect(), productOrderController.delete);

app.post('/api/login', authenticationController.login);

app.get('/', (req, res) => res.render('login'));
app.post('/orders', authenticationController.loginBrowser);

app.listen(3001, () => {
  console.log('Listening on 3001...');
});
