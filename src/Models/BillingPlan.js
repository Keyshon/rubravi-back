import mongoose from 'mongoose';

const BillingPlanSchema = new mongoose.Schema(
  {
    name: String,
    cost: Number
  },
  {
    timestamps: true
  }
);

const BillingPlan = mongoose.model('BillingPlan', BillingPlanSchema);

export default BillingPlan;
