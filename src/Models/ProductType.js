import mongoose from 'mongoose';

const ProductTypeSchema = new mongoose.Schema(
  {
    name: String,
    costMonth: Number,
    costHalfYear: Number,
    costYear: Number,
    supportTerm: Number
  },
  {
    timestamps: true
  }
);

const ProductType = mongoose.model('ProductType', ProductTypeSchema);

export default ProductType;
