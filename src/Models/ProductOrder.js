import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ProductOrderSchema = new Schema(
  {
    productType: { type: Schema.Types.ObjectId, ref: 'ProductType' },
    userName: String,
    userPhone: String,
    userEmail: String,
    subscriptionType: { type: Schema.Types.ObjectId, ref: 'SubscriptionType' },
    subscriptionTerm: Number
  },
  {
    timestamps: true
  }
);

const ProductOrder = mongoose.model('ProductOrder', ProductOrderSchema);

export default ProductOrder;
