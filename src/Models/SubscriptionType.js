import mongoose from 'mongoose';

const SubscriptionTypeSchema = new mongoose.Schema(
  {
    name: String,
    term: Number
  },
  {
    timestamps: true
  }
);

const SubscriptionType = mongoose.model(
  'SubscriptionType',
  SubscriptionTypeSchema
);

export default SubscriptionType;
