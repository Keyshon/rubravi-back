const credentials = require('./../con');
const nodemailer = require('nodemailer');

async function sendEmail(to, subject, html) {
  let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: credentials.credentials.username,
      pass: credentials.credentials.password
    }
  });

  const mail = {
    from: `"No-reply RuBraVi" <${credentials.credentials.username}>`,
    to,
    subject,
    html
  };

  transporter.sendMail(mail, (err, data) => {
    if (err) console.log('Error: ', err);
    else console.log('Email sent');
  });
}

export default sendEmail;
